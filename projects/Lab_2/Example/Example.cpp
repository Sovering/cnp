#include <stdio.h>
#include "WindowManager.h"


void OnWindowKeyPressed(GLuint KeySym, GLboolean KeyState)
{
    if(KeySym == ' ' && KeyState == KEYSTATE_DOWN)
	{
		printf("1234\n");
	}
}


int main()
{
	WindowManager::Initialize();
	WindowManager::AddWindow(new FWindow("Example"));
	WindowManager::GetWindowByName("Example")->PrintOpenGLVersion();
	WindowManager::SetWindowOnKeyEvent("Example", &OnWindowKeyPressed);

	while (!WindowManager::GetWindowShouldClose("Example"))
	{
		WindowManager::PollForEvents(); // or waitForEvents
		for (GLuint i = 0; i < WindowManager::GetNumWindows(); i++,)
		{
			WindowManager::GetWindowByIndex(i)->MakeCurrentContext();
			glClearColor(0.25f, 0.25f, 0.25f, 0.25f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			WindowManager::GetWindowByIndex(i)->SwapDrawBuffers();
		}
	}	

	WindowManager::ShutDown();
	return 0;
}

#include "WindowManager.h"
#include <cstring>


WindowManager::WindowManager()
{
	//GetInstance()->Initialized = GL_FALSE;
}


GLboolean WindowManager::Initialize()
{
	GetInstance()->Initialized = GL_FALSE;
#if defined(_WIN32) || defined(_WIN64)
	return Windows_Initialize();
#else
	return Linux_Initialize();
#endif
}

GLboolean WindowManager::IsInitialized()
{
	return GetInstance()->Initialized;
}


WindowManager::~WindowManager()
{
	if (!GetInstance()->Windows.empty())
	{
#if defined(_MSC_VER)
		for each(auto CurrentWindow in GetInstance()->Windows)
#else
		for (auto CurrentWindow : GetInstance()->Windows)
#endif
		{
			delete CurrentWindow;
		}
		GetInstance()->Windows.clear();
	}
}


FWindow* WindowManager::GetWindowByName(const char* WindowName)
{
	if (DoesExist(WindowName))
	{
#if defined(_MSC_VER)
		for each(auto CurrentWindow in GetInstance()->Windows)
#else
		for (auto CurrentWindow : GetInstance()->Windows)
#endif
		{
			if (!strcmp(CurrentWindow->Name, WindowName))
			{
				return CurrentWindow;
			}
		}
		PrintErrorMessage(ERROR_WINDOWNOTFOUND);
		return nullptr;
	}
	return nullptr;
}


FWindow* WindowManager::GetWindowByIndex(GLuint WindowIndex)
{
	if (DoesExist(WindowIndex))
	{
#if defined(_MSC_VER)
		for each (auto CurrentWindow in GetInstance()->Windows)
#else
		for (auto CurrentWindow : GetInstance()->Windows)
#endif
		{
			if(CurrentWindow->ID == WindowIndex)
			{
				return CurrentWindow;
			}
		}

		PrintErrorMessage(ERROR_WINDOWNOTFOUND);
		return nullptr;
	}

	return FOUNDATION_ERROR;
}


WindowManager* WindowManager::AddWindow(FWindow* NewWindow)
{
	if (GetInstance()->IsInitialized())
	{
		if (NewWindow != nullptr)
		{
			GetInstance()->Windows.push_back(NewWindow);
			NewWindow->ID = GetInstance()->Windows.size() - 1;
			NewWindow->Initialize();
			return GetInstance();
		}
		PrintErrorMessage(ERROR_INVALIDWINDOW);
		return nullptr;
	}
	PrintErrorMessage(ERROR_NOTINITIALIZED);
	return nullptr;
}


WindowManager* WindowManager::GetInstance()
{
	if(!WindowManager::Instance)
	{
		WindowManager::Instance = new WindowManager();
		return WindowManager::Instance;
	}

	else 
	{		
		return WindowManager::Instance;
	}
}


GLboolean WindowManager::DoesExist(const char* WindowName)
{
	if (GetInstance()->IsInitialized())
	{
		if (IsValidString(WindowName))
		{
#if defined(_MSC_VER)
			for each(auto iter in GetInstance()->Windows)
#else
			for (auto iter : GetInstance()->Windows)
#endif
			{
				if (iter->Name == WindowName)
				{
					return GL_TRUE;
				}
			}
		}
		PrintErrorMessage(ERROR_INVALIDWINDOWNAME);
		return GL_FALSE;
	}
	return GL_FALSE;
}


GLboolean WindowManager::DoesExist(GLuint WindowIndex)
{
	if (GetInstance()->IsInitialized())
	{
		if (WindowIndex <= (GetInstance()->Windows.size() - 1))
		{
			return FOUNDATION_OKAY;
		}

		PrintErrorMessage(ERROR_INVALIDWINDOWINDEX);
		return FOUNDATION_ERROR;
	}
	return FOUNDATION_ERROR;
}


GLuint WindowManager::GetNumWindows()
{
	if(GetInstance()->IsInitialized())
	{
		return GetInstance()->Windows.size();
	}

	PrintErrorMessage(ERROR_NOTINITIALIZED);
	return FOUNDATION_ERROR;
}


void WindowManager::ShutDown()
{	
#if defined(_MSC_VER)
	for each(auto CurrentWindow in GetInstance()->Windows)
	{
		delete CurrentWindow;
	}
#endif

#if defined(CURRENT_OS_LINUX)
	for (auto CurrentWindow : GetInstance()->Windows)
	{
		delete CurrentWindow;
	}
	XCloseDisplay(GetInstance()->m_Display);
#endif

	GetInstance()->Windows.clear();

	delete Instance;
}


GLboolean WindowManager::GetMousePositionInScreen(GLuint& X, GLuint& Y)
{
	if (GetInstance()->IsInitialized())
	{
		X = GetInstance()->ScreenMousePosition[0];
		Y = GetInstance()->ScreenMousePosition[1];
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOTINITIALIZED);
	return FOUNDATION_ERROR;

}


GLuint* WindowManager::GetMousePositionInScreen()
{
	if (GetInstance()->IsInitialized())
	{
		return GetInstance()->ScreenMousePosition;
	}

	PrintErrorMessage(ERROR_NOTINITIALIZED);
	return nullptr;
}


GLboolean WindowManager::SetMousePositionInScreen(GLuint X, GLuint Y)
{
	GetInstance()->ScreenMousePosition[0] = X;
	GetInstance()->ScreenMousePosition[1] = Y;
#if defined(_WIN32) || defined(_WIN64)
	return Windows_SetMousePositionInScreen(X, Y);
#else
	return Linux_SetMousePositionInScreen(X, Y);
#endif
}



GLuint* WindowManager::GetScreenResolution()
{
	if (GetInstance()->IsInitialized())
	{
#if defined(_WIN32) || defined(_WIN64)
		RECT l_Screen;
		HWND m_Desktop = GetDesktopWindow();
		GetWindowRect(m_Desktop, &l_Screen);

		GetInstance()->ScreenResolution[0] = l_Screen.right;
		GetInstance()->ScreenResolution[1] = l_Screen.bottom;
		return GetInstance()->ScreenResolution;
#endif

#if defined(__linux__)
		GetInstance()->ScreenResolution[0] = WidthOfScreen(XDefaultScreenOfDisplay(GetInstance()->m_Display));
		GetInstance()->ScreenResolution[1] = HeightOfScreen(XDefaultScreenOfDisplay(GetInstance()->m_Display));

		return GetInstance()->ScreenResolution;
#endif
	}
	PrintErrorMessage(ERROR_NOTINITIALIZED);
	return nullptr;

}

GLboolean WindowManager::PollForEvents()
{
	if (GetInstance()->IsInitialized())
	{
#if defined(_WIN32) || defined(_WIN64)
		return GetInstance()->Windows_PollForEvents();
#else
		return GetInstance()->Linux_PollForEvents();
#endif	
	}

	PrintErrorMessage(ERROR_NOTINITIALIZED);
	return FOUNDATION_ERROR;
}

GLboolean WindowManager::WaitForEvents()
{
	if (GetInstance()->IsInitialized())
	{
#if defined(_WIN32) || defined(_WIN64)
		return GetInstance()->Windows_WaitForEvents();
#else
		return GetInstance()->Linux_WaitForEvents();
#endif
	}

	PrintErrorMessage(ERROR_NOTINITIALIZED);
	return FOUNDATION_ERROR;
}

GLboolean WindowManager::GetScreenResolution(GLuint& Width, GLuint& Height)
{
	if (GetInstance()->IsInitialized())
	{
#if defined(_WIN32) || defined(_WIN64)

		RECT l_Screen;
		HWND m_Desktop = GetDesktopWindow();
		GetWindowRect(m_Desktop, &l_Screen);
		Width = l_Screen.right;
		Height = l_Screen.bottom;
#endif

#if defined(__linux__)

		Width = WidthOfScreen(XDefaultScreenOfDisplay(GetInstance()->m_Display));
		Height = HeightOfScreen(XDefaultScreenOfDisplay(GetInstance()->m_Display));


		GetInstance()->ScreenResolution[0] = Width;
		GetInstance()->ScreenResolution[1] = Height;
#endif

		return FOUNDATION_OKAY;
	}
	PrintErrorMessage(ERROR_NOTINITIALIZED);
	return FOUNDATION_ERROR;
}


GLboolean WindowManager::GetWindowResolution(const char* WindowName, GLuint& Width, GLuint& Height)
{
	if (GetInstance()->IsInitialized())
	{
		if (DoesExist(WindowName))
		{
			if (GetWindowByName(WindowName)->GetResolution(Width, Height))
			{
				return FOUNDATION_OKAY;
			}
			return FOUNDATION_ERROR;
		}
		return FOUNDATION_ERROR;
	}

	PrintErrorMessage(ERROR_NOTINITIALIZED);
	return FOUNDATION_ERROR;
}


GLboolean WindowManager::GetWindowResolution(GLuint WindowIndex, GLuint& Width, GLuint& Height)
{
	if (DoesExist(WindowIndex))
	{
		GetWindowByIndex(WindowIndex)->GetResolution(Width, Height);
		return FOUNDATION_OKAY;
	}

	PrintErrorMessage(ERROR_NOTINITIALIZED);
	return FOUNDATION_ERROR;
}


GLuint* WindowManager::GetWindowResolution(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetResolution();
	}

	return nullptr;
}


GLuint* WindowManager::GetWindowResolution(GLuint WindowIndex)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->GetResolution();
	}

	return nullptr;
}


GLboolean WindowManager::SetWindowResolution(const char* WindowName, GLuint Width, GLuint Height)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetResolution(Width, Height);
	}

	return GL_FALSE;
}


GLboolean WindowManager::SetWindowResolution(GLuint WindowIndex, GLuint Width, GLuint Height)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetResolution(Width, Height);
	}

	return GL_FALSE;
}


GLboolean WindowManager::GetWindowPosition(const char* WindowName, GLuint& X, GLuint& Y)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetPosition(X, Y);
	}

	return GL_FALSE;
}


GLboolean WindowManager::GetWindowPosition(GLuint WindowIndex, GLuint& X, GLuint& Y)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->GetPosition(X, Y);
	}

	return GL_FALSE;
}


GLuint* WindowManager::GetWindowPosition(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetPosition();
	}

	return nullptr;
}


GLuint* WindowManager::GetWindowPosition(GLuint WindowIndex)
{
	if(WindowIndex <= GetInstance()->Windows.size() -1)
	{
		return GetWindowByIndex(WindowIndex)->GetPosition();
	}

	return nullptr;
}


GLboolean WindowManager::SetWindowPosition(const char* WindowName, GLuint X, GLuint Y)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetPosition(X, Y);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowPosition(GLuint WindowIndex, GLuint X, GLuint Y)
{
	if(WindowIndex <= GetInstance()->Windows.size() -1)
	{
		return GetWindowByIndex(WindowIndex)->SetPosition(X, Y);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::GetMousePositionInWindow(const char* WindowName, GLuint& X, GLuint& Y)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetMousePosition(X, Y);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::GetMousePositionInWindow(GLuint WindowIndex, GLuint& X, GLuint& Y)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->GetMousePosition(X, Y);
	}

	return FOUNDATION_ERROR;
}


GLuint* WindowManager::GetMousePositionInWindow(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetMousePosition();
	}

	return nullptr;
}


GLuint* WindowManager::GetMousePositionInWindow(GLuint WindowIndex)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->GetMousePosition();
	}
	PrintErrorMessage(ERROR_INVALIDWINDOWINDEX);
	return nullptr;
}


GLboolean WindowManager::SetMousePositionInWindow(const char* WindowName, GLuint X, GLuint Y)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetMousePosition(X, Y);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetMousePositionInWindow(GLuint WindowIndex, GLuint X, GLuint Y)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetMousePosition(X, Y);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::WindowGetKey(const char* WindowName, GLuint Key)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetKeyState(Key);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::WindowGetKey(GLuint WindowIndex, GLuint Key)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->GetKeyState(Key);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::GetWindowShouldClose(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetShouldClose();
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::GetWindowShouldClose(GLuint WindowIndex)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->GetShouldClose();
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::WindowSwapBuffers(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SwapDrawBuffers();
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::WindowSwapBuffers(GLuint WindowIndex)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SwapDrawBuffers();
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::GetWindowIsFullScreen(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetIsFullScreen();
	}
	
	return FOUNDATION_ERROR;
}


GLboolean WindowManager::GetWindowIsFullScreen(GLuint WindowIndex)
{
	if(WindowIndex <= GetInstance()->Windows.size() -1)
	{
		return GetWindowByIndex(WindowIndex)->GetIsFullScreen();
	}
	
	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetFullScreen(const char* WindowName, GLboolean ShouldBeFullscreen)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->FullScreen(ShouldBeFullscreen);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetFullScreen(GLuint WindowIndex, GLboolean ShouldBeFullscreen)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->FullScreen(ShouldBeFullscreen);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::GetWindowIsMinimized(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetIsMinimized();
	}
	
	return FOUNDATION_ERROR;
}


GLboolean WindowManager::GetWindowIsMinimized(GLuint WindowIndex)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->GetIsMinimized();
	}
	
	return FOUNDATION_ERROR;
}



GLboolean WindowManager::MinimizeWindow(const char* WindowName, GLboolean ShouldBeMinimized)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->FullScreen(ShouldBeMinimized);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::MinimizeWindow(GLuint WindowIndex, GLboolean ShouldBeMinimized)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->FullScreen(ShouldBeMinimized);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::GetWindowIsMaximized(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetIsMaximized();
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::GetWindowIsMaximized(GLuint WindowIndex)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->GetIsMaximized();
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::MaximizeWindow(const char* WindowName, GLboolean ShouldBeMaximized)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->FullScreen(ShouldBeMaximized);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::MaximizeWindow(GLuint WindowIndex, GLboolean ShouldBeMaximized)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->FullScreen(ShouldBeMaximized);
	}

	return FOUNDATION_ERROR;
}


const char* WindowManager::GetWindowName(GLuint WindowIndex)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->GetWindowName();
	}

	return nullptr;
}


GLuint WindowManager::GetWindowIndex(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->ID;
	}

	return 0;
}


GLboolean WindowManager::SetWindowTitleBar(const char* WindowName, const char* NewTitle)
{
	if(DoesExist(WindowName) && IsValidString(NewTitle))
	{
		return GetWindowByName(WindowName)->SetTitleBar(NewTitle);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::SetWindowTitleBar(GLuint WindowIndex, const char* NewTitle)
{
	if(DoesExist(WindowIndex) && IsValidString(NewTitle))
	{
		return GetWindowByIndex(WindowIndex)->SetTitleBar(NewTitle);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::GetWindowIsInFocus(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->GetInFocus();
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::GetWindowIsInFocus(GLuint WindowIndex)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->GetInFocus();
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::FocusWindow(const char* WindowName, GLboolean ShouldBeFocused)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->Focus(ShouldBeFocused);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::FocusWindow(GLuint WindowIndex, GLboolean ShouldBeFocused)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->Focus(ShouldBeFocused);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::RestoreWindow(const char* WindowName)
{
	if(DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->Restore();
	}
	return FOUNDATION_ERROR;
	//implement window focusing
}



GLboolean WindowManager::RestoreWindow(GLuint WindowIndex)
{
	if(DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->Restore();
	}

	return FOUNDATION_ERROR;
	//implement window focusing
}



GLboolean WindowManager::SetWindowSwapInterval(const char* WindowName, GLint a_SyncSetting)
{
	if (DoesExist(WindowName))
	{
		 return GetWindowByName(WindowName)->SetSwapInterval(a_SyncSetting);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowSwapInterval(GLuint WindowIndex, GLint a_SyncSetting)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetSwapInterval(a_SyncSetting);
	}

	return FOUNDATION_ERROR;
}

GLboolean WindowManager::SetWindowStyle(const char* WindowName, GLuint WindowStyle)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetStyle(WindowStyle);
	}

	return FOUNDATION_ERROR;
}

GLboolean WindowManager::SetWindowStyle(GLuint WindowIndex, GLuint WindowStyle)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetStyle(WindowStyle);
	}

	return FOUNDATION_ERROR;
}

GLboolean WindowManager::EnableWindowDecorator(const char* WindowName, GLbitfield Decorators)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->EnableDecorator(Decorators);
	}

	return FOUNDATION_ERROR;

}

GLboolean WindowManager::EnableWindowDecorator(GLuint WindowIndex, GLbitfield Decorators)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->EnableDecorator(Decorators);
	}

	return FOUNDATION_ERROR;
}

GLboolean WindowManager::DisableWindowDecorator(const char* WindowName, GLbitfield Decorators)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->DisableDecorator(Decorators);
	}

	return FOUNDATION_ERROR;
}

GLboolean WindowManager::DisableWindowDecorator(GLuint WindowIndex, GLbitfield Decorators)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->DisableDecorator(Decorators);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::SetWindowOnKeyEvent(const char* WindowName, OnKeyEvent OnKey)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetOnKeyEvent(OnKey);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnKeyEvent(GLuint WindowIndex, OnKeyEvent OnKey)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetOnKeyEvent(OnKey);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnMouseButtonEvent(const char* WindowName, OnMouseButtonEvent OnMouseButton)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetOnMouseButtonEvent(OnMouseButton);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnMouseButtonEvent(GLuint WindowIndex, OnMouseButtonEvent OnMouseButton)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetOnMouseButtonEvent(OnMouseButton);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::SetWindowOnMouseWheelEvent(const char* WindowName, OnMouseWheelEvent OnMouseWheel)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetOnMouseWheelEvent(OnMouseWheel);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnMouseWheelEvent(GLuint WindowIndex, OnMouseWheelEvent OnMouseWheel)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetOnMouseWheelEvent(OnMouseWheel);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnDestroyed(const char* WindowName, OnDestroyedEvent OnDestroyed)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetOnDestroyed(OnDestroyed);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::SetWindowOnDestroyed(GLuint WindowIndex, OnDestroyedEvent OnDestroyed)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetOnDestroyed(OnDestroyed);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnMaximized(const char* WindowName, OnMaximizedEvent OnMaximized)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetOnMaximized(OnMaximized);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnMaximized(GLuint WindowIndex, OnMaximizedEvent OnMaximized)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetOnMaximized(OnMaximized);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnMinimized(const char* WindowName, OnMinimizedEvent OnMinimized)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetOnMinimized(OnMinimized);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnMinimized(GLuint WindowIndex, OnMinimizedEvent OnMinimized)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetOnMinimized(OnMinimized);
	}

	return FOUNDATION_ERROR;
}

/*void WindowManager::SetWindowOnRestored(const char* WindowName, OnRestoredEvent OnRestored)
{
	if (DoesExist(WindowName))
	{
		GetWindowByName(WindowName)->SetOnRestored(OnRestored);
	}
}

void WindowManager::SetWindowOnRestored(GLuint WindowIndex, OnRestoredEvent OnRestored)
{
	if (DoesExist(WindowIndex))
	{
		GetWindowByIndex(WindowIndex)->SetOnRestored(OnRestored);
	}
}*/



GLboolean WindowManager::SetWindowOnFocus(const char* WindowName, OnFocusEvent OnFocus)
{
	if(IsValidString(WindowName))
	{
		GetWindowByName(WindowName)->FocusEvent = OnFocus;
		return FOUNDATION_OKAY;
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::SetWindowOnFocus(GLuint WindowIndex, OnFocusEvent OnFocus)
{
	if(DoesExist(WindowIndex))
	{
		GetWindowByIndex(WindowIndex)->FocusEvent = OnFocus;
		return FOUNDATION_OKAY;
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnMoved(const char* WindowName, OnMovedEvent OnMoved)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetOnMoved(OnMoved);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnMoved(GLuint WindowIndex, OnMovedEvent OnMoved)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetOnMoved(OnMoved);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnResize(const char* WindowName, OnResizeEvent OnResize)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetOnResize(OnResize);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnResize(GLuint WindowIndex, OnResizeEvent OnResize)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetOnResize(OnResize);
	}

	return FOUNDATION_ERROR;
}



GLboolean WindowManager::SetWindowOnMouseMove(const char* WindowName, OnMouseMoveEvent OnMouseMove)
{
	if (DoesExist(WindowName))
	{
		return GetWindowByName(WindowName)->SetOnMouseMove(OnMouseMove);
	}

	return FOUNDATION_ERROR;
}


GLboolean WindowManager::SetWindowOnMouseMove(GLuint WindowIndex, OnMouseMoveEvent OnMouseMove)
{
	if (DoesExist(WindowIndex))
	{
		return GetWindowByIndex(WindowIndex)->SetOnMouseMove(OnMouseMove);
	}

	return FOUNDATION_ERROR;
}

WindowManager* WindowManager::Instance = 0;
